FROM node:16.15.0-alpine

ENTRYPOINT [ "env" ]

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE ${PORT}

CMD [ "npm", "start" ]
