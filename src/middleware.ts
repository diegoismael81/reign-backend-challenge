import { Request, Response, NextFunction } from "express";
import jwt from 'jsonwebtoken';

export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
    const { authorization } = req.headers;
    try {        
        if (!authorization) return res.status(401).json({message : 'Access Denied'});        
        jwt.verify(authorization, process.env.TOKEN_SECRET || '');        
        next();
    } catch (e) {
        res.status(400).json({message : 'Access Denied'});
    }
}