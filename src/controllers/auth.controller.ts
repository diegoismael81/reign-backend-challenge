import { Request, Response } from "express";
import { Get, Route } from "tsoa";
import jwt from 'jsonwebtoken';
import { IResponse } from "../models/response.model";


export const getToken = async (req: Request, res: Response) => {
  const controller = new AuthController();
  const response = await controller.getToken(); 
  res.header('auth-token', response.value?.toString() ).json(response);    
};


@Route("auth")
class AuthController {

    @Get("/token")
    public async getToken(): Promise<IResponse> {
        const token: string = jwt.sign({ name: "Anonymous"  }, process.env['TOKEN_SECRET'] || '', {
            expiresIn: 15 * 60 * 1000
        });
      return {
        message: "JWT success",
        value : token
      };
    }
  }