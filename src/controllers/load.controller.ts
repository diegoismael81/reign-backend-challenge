import { HitElement } from "../models/hit.model";

export async function loadHits() {           
    try{
        const axios = require('axios');
        const response = await axios.get(process.env.DATA_URI);
        console.log(response);
        const data = response.data;
        for(const hit of data.hits) {
            const { created_at, title, url, author, points, story_text, comment_text, num_comments, story_id, story_title, story_url, parent_id, created_at_i, _tags, objectID } = hit;
            const hitElement = new HitElement({ created_at, title, url, author, points, story_text, comment_text, num_comments, story_id, story_title, story_url, parent_id, created_at_i, _tags, objectID });
            await hitElement.save();
            console.log(`Hit id: ${story_id}, author: ${author}`);
        }
        const docsSaved = data.hits.length;
        console.log(`${docsSaved} hits saved!`);
    }
    catch(err){
        console.error(err);
    }
}