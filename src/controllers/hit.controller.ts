import { Get, Route, Path, Body, Post } from "tsoa";
import { Request, Response } from "express";
import { HitElement } from "../models/hit.model";
import { IResponseHits } from "../models/response.model";

const monthNames = ["january", "february", "march", "april", "may", "june",
  "july", "august", "september", "october", "november", "december"];

  export const getByAuthor = async (req: Request, res: Response) => {           
    try{
        let page = parseInt(req.params.page);                        
        const response = await new HitController().getByAuthor(req.params.author, page);         
        return res.status(200).json(response);
    }
    catch(err){
        return res.status(500).json(err);
    }
}

export const getByTitle = async (req: Request, res: Response)=> {           
    try{        
        let page = parseInt(req.params.page);                        
        const response = await new HitController().getByTitle(req.body.titles, page);         
        return res.status(200).json(response);
    }
    catch(err){        
        return res.status(500).json(err);
    }
}

export const getByTags = async (req: Request, res: Response) => {           
    try{        
        let page = parseInt(req.params.page);        
        const response = await new HitController().getByTags(req.body.tags, page);                 
        return res.status(200).json(response);
    }
    catch(err){
        return res.status(500).json(err);
    }
}

export const getByMonth = async (req: Request, res: Response)=> {           
    try{                
        let page = parseInt(req.params.page);                       
        const response = await new HitController().getByMonth(req.params.month, page);         
        return res.status(200).json(response);
    }
    catch(err){        
        return res.status(500).json(err);
    }
}

@Route("hits")
class HitController {
    @Get("/author/:author/:page")
    public async getByAuthor(@Path() author: string, @Path() page: number): Promise<IResponseHits> {
        let skip = page == 1 ? 0 : (page - 1) * 5;
        const data = await HitElement.find({ author : author }).skip(skip).limit(5);
      return {
        message: "Get all hits by author",
        hits : data
      };
    }

    @Post("/titles/:page")
    public async getByTitle(@Body() titles: Array<string>, @Path() page: number): Promise<IResponseHits> {
      var query = {
        story_title : {}
    };
    var payload = {"title": titles };
    if (payload.title && payload.title.length > 0) query.story_title = {$in : payload.title};  
      let skip = page == 1 ? 0 : (page - 1) * 5;
        const data = await HitElement.find(query).skip(skip).limit(5);
      return {
        message: "Get all hits by title",
        hits : data
      };
    }

    @Get("/month/:month/:page")
    public async getByMonth(@Path() month: string, @Path() page: number): Promise<IResponseHits> {
        let monthNumber = monthNames.indexOf(month.toLowerCase()) + 1;
        const docsIds = await HitElement.aggregate([
            {$project: {
                id: 1,
                month: {$month: '$created_at'}
            }},
            {$match: {month: monthNumber}},
            {$project: {
                id: 1
            }}
        ]);
        let skip = page == 1 ? 0 : (page - 1) * 5;
        const data = await HitElement.find({ '_id': { $in: docsIds } }).skip(skip).limit(5);
      return {
        message: "Get all hits by month's name",
        hits : data
      };
    }

    @Post("/tags/:page")
    public async getByTags(@Body() tags: Array<string>, @Path() page: number): Promise<IResponseHits> {
        var query = {
            _tags : {}
        };
        var payload = {"tags": tags };
        if (payload.tags && payload.tags.length > 0) query._tags = {$in : payload.tags};
        let skip = page == 1 ? 0 : (page - 1) * 5;
        const data = await HitElement.find(query).skip(skip).limit(5);
      return {
        message: "Get all hits by tags",
        hits : data
      };
    }


  }

