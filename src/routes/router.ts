import {Application} from 'express';
import { getToken } from '../controllers/auth.controller';
import { getByAuthor, getByMonth, getByTags, getByTitle } from '../controllers/hit.controller';
import { isAuthenticated } from '../middleware';

export function routes(app: Application){    
    app.get('/hits/author/:author/:page', [ isAuthenticated, getByAuthor ]  );
    app.post('/hits/titles/:page',  [ isAuthenticated, getByTitle ] ); 
    app.get('/hits/month/:month/:page', [ isAuthenticated, getByMonth] );   
    app.post('/hits/tags/:page', [ isAuthenticated, getByTags] );
    app.get('/auth/token', getToken)         
}