import express from 'express';
import { MIN_START, PORT } from './config';
import { loadHits } from './controllers/load.controller';
import { connectToMongodb } from './database';
import { routes } from './routes/router';
import cron from 'node-cron';
import swaggerUi from "swagger-ui-express";

//=== Start Server ====
async function main() {
    await connectToMongodb();
    const server = express();
    server.use(express.json());
    server.use(express.static("public"));

    //=== Swagger ====
    server.use(
        "/docs",
        swaggerUi.serve,
        swaggerUi.setup(undefined, {
          swaggerOptions: {
            url: "/swagger.json",
          },
        })
    );
    
    routes(server);
    server.listen(PORT, () => {
        console.log('The application is listening on port 3000!');
    })
}

// ==== Load data from external URI ===
cron.schedule(`${MIN_START} * * * *`, async () => {
    await loadHits();
});
  
main();