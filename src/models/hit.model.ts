import { Schema, model } from 'mongoose';

//==== HitElement Model ===
//Interface
export interface IHitElement {
    created_at:       Date;
    title:            null | string;
    url:              null | string;
    author:           string;
    points:           number | null;
    story_text:       null | string;
    comment_text:     null | string;
    num_comments:     number | null;
    story_id:         number | null;
    story_title:      null | string;
    story_url:        null | string;
    parent_id:        number | null;
    created_at_i:     number;
    _tags:            string[];
    objectID:         string
}

//Schema
const hitElementSchema = new Schema<IHitElement>({
    created_at:       {type: Date},
    title:            {type: String},
    url:              {type: String},
    author:           {type: String},
    points:           {type: Number},
    story_text:       {type: String},
    comment_text:     {type: String},
    num_comments:     {type: Number},
    story_id:         {type: Number},
    story_title:      {type: String},
    story_url:        {type: String},
    parent_id:        {type: Number},
    created_at_i:     {type: Number},
    _tags:            [{type: String}],
    objectID:         {type: String}
});

//Model
const HitElement = model<IHitElement>('HitElement', hitElementSchema);

export {HitElement}
