# Reign BackEnd Challenge

This is a repository to Back End Developer Challenge for Reign

## STACK
 - Node.js Active LTS 16.15.0
 - Typescript
 - MongoDB
 - ORM: Mongoose
 - Swagger
 - Docker

# DEVELOPMENT
- In order to load data from external URI, It's using a function as a job with node-cron scheduled it on a defined minute by hour, this function calls another one thats load tha data using axios and saved it on mongodb docs database.

- The server was created with Express.

- In order to defined the database structure, It's using Mongosse with interfaces, Schemas, and models; this Schema can be used to query the docs by criteria filters.

- It's created a controller to Query docs, by various options that using the method find of the Schema to filter the data. Inside this controller it has some express methods to manage Request/Response and a class to make the quieres by Mongoose.

- There is an especial method that queries the hits by month's name, It use a Array of string to determinate the number of month and use aggregate Mongoose function to get the ids docs, after that, these ids docs were finding to retrieve the data.

- To query by tags & titles, It's using POST cause both send request body.

- To make an authentication emulation, It's using a controller to generate a token by jsonwebtoken and a middleware to check the access to all routes.

- There is a router file to managment all paths available on the API, except one that create the token.

- There is a env file to keep some String like the TOKEN_SECRET, MIN_START, external DATA_URI, etc.

- To documentation the API Rest, It's using Swagger with nodemon & tsoa, It was declared into scritps on package.json, also It's using decorations to classes, functions and parameters to generate Swagger.

# DEPLOYMENT
- Dockerfile is using to build an image, It has commands to load the Node Image, install dependencies, build Typescript, and run the Nodejs server.

- docker-compose is using to create a net integration to nodejs and mongodb


# Thanks
I'm very happy to make this code challenge, Thanks a lot by the oportunity.